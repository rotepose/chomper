![Rotepose](http://rotepose.com/app/pics/skul.ico)

# Chomper
> if you like nibbler, you'll love chomper!

a canvas snake game with cgi (high scores, levels...)

## Installation

it's web-based. If you like it, clone and serve.

## Usage

It's got a picture currently, so you can enjoy that. At first it will be keyboard only, with goals to add swipe functionality for phones.

## Links

- Project Homepage: https://gitlab.com/rotepose/chomper/
- Other Rotepose efforts:
  - a screensaver for terminal: https://gitlab.com/rotepose23/shmatrix/
  - an commandline todo app: https://gitlab.com/rotepose23/todo/

## License

[BSD-3-Clause](https://opensource.org/licenses/BSD-3-Clause)
