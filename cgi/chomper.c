#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

void parseQ(const char*, char*, char*);

int
main(int argc, char **argv) {
/*{{{*/

	char width[8] = {0};
	char height[8] = {0};
	const char *q = NULL;

	q = getenv("QUERY_STRING");
	parseQ(q, width, height);

	printf(" ! chomper - if you like nibbler, you\'ll love  chomper!\n");
	printf(" !\n");
	printf(" !>\n");
	printf("<!doctype html>\n");
	printf("\n");
	printf("<html lang=\"en\">\n");
	printf("\n");
	printf("<head>\n");
	printf("  <meta charset=\"utf-8\" />\n");
	printf("  <title>chomper</title>\n");
	printf("  <link rel=\"shortcut icon\" href=\"../pics/icon.png\" />\n");
	printf("  <link rel=StyleSheet href=\"../css/chomper.css\" type=\"text/css\" media=screen />\n");
	printf("  <script type=\"text/javascript\" src=\"../js/chomper.js\"></script>\n");
	printf("</head>\n");
	printf("\n");
	printf("<body>\n");
	printf("  <div id=\"v\">\n");
	printf("    <div id=\"h\">\n");
	// this is where you would change the dimensions via callback
	printf("      <canvas id=\"c\" width=\"%s\" height=\"%s\"></canvas>\n", width, height);
	printf("    </div>\n");
	printf("  </div>\n");
	printf("</body>\n");
	printf("\n");
	printf("</html>\n");

	return 0;

}/*}}}*/

// parses query string
void
parseQ(const char *q, char *w, char *h) {
/*{{{*/
	int i = 0, a = 0;

	while ( q[i++] != '\0' ) {
		if ( w[a] == 0 ) {
			while ( isdigit(q[i]) ) {
				w[a++] = q[i++];
			}
			a = 0;
		}
		if ( h[a] == 0 ) {
			while ( isdigit(q[i]) ) {
				h[a++] = q[i++];
			}
		}
	}

	return;

}/*}}}*/
