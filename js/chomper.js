/*
 * Author: Jeremy Cartwright
 * chomper: if you like nibbler, you'll love chomper!
 *
 */

function init() {

	var cvs = document.getElementById('c');
	var ctx = cvs.getContext('2d');
	var headRadius = 20;
	//var x = cvs.width/2;
	//var y = cvs.height-300;
	var grid = [];
	for(h=0; h<cvs.width/(headRadius*2); h++) {
		grid[h] = [];
		for(v=0; v<cvs.height/(headRadius*2); v++) { 
			grid[h][v] = { x: (h*(headRadius*2))+headRadius, y: (v*(headRadius*2))+headRadius };
		}
	}

	function play() {

		ctx.clearRect(0, 0, cvs.width, cvs.height);
		
		drawHead();
		
		var doIt = requestAnimationFrame(play);

	}
	
	function drawHead() {

		// noggin
		h = 9;
		v = 4;
		ctx.beginPath();
		ctx.arc(grid[h][v].x, grid[h][v].y, headRadius, 0, Math.PI*2);
		ctx.fillStyle = '#803300';
		ctx.fill();
		ctx.closePath();

		// mouth
		//ctx.beginPath();

		// eyes
		ctx.beginPath();
		ctx.arc(grid[h][v].x-7, grid[h][v].y-3, headRadius/4, 0, Math.PI*2);
		ctx.arc(grid[h][v].x+7, grid[h][v].y-3, headRadius/4, 0, Math.PI*2);
		ctx.fillStyle = '#000000';
		ctx.fill();
		ctx.closePath();
		ctx.beginPath();
		ctx.arc(grid[h][v].x-6, grid[h][v].y-5, headRadius/12, 0, Math.PI*2);
		ctx.arc(grid[h][v].x+8, grid[h][v].y-5, headRadius/12, 0, Math.PI*2);
		ctx.fillStyle = '#EEEEEE';
		ctx.fill();
		ctx.closePath();
	}

	function direction(event) {
		let key = event.keyCode;
		if(key == 68 && d != "LEFT"){
			d = "RIGHT";}
		else if(key == 65 && d != "RIGHT"){
			d = "LEFT";}
		else if(key == 87 && d != "DOWN"){
			d = "UP";}
		else if(key == 83 && d != "UP"){
			d = "DOWN";}
	}
	
	function gameOver() {
		cancelAnimationFrame(doIt);
	}
	
	play();

}

window.onload=init;
