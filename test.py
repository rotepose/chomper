import http.server
import socketserver

PORT = 8000

Handler = http.server.CGIHTTPRequestHandler

Handler.cgi_directories = ['/cgi']

with http.server.HTTPServer(("", PORT), Handler) as httpd:
    print("serving at port", PORT)
    httpd.serve_forever()
